﻿namespace RabbitSender
{
    class Program
    {
        private static readonly string queueName = "HelloRabbitMQ";

        static void Main(string[] args)
        {
            var rabbitSender = new Sender(queueName);
            rabbitSender.Do();
        }
    }
}

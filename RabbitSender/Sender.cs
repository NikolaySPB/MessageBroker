﻿using DataShipping;
using RabbitShell;
using System.Threading;

namespace RabbitSender
{
    public class Sender
    {
        private readonly Rabbit _rabbit;
        public Sender(string queueName)
        {
            _rabbit = new Rabbit(queueName);
        }

        public void Do()
        {
            while (true)
            {
                var faker = new Bogus.Faker();
                var userFullName = faker.Name.FullName();
                var user = new User()
                {
                    Age = faker.Random.Int(10, 110),
                    Email = faker.Internet.Email(userFullName),
                    Name = userFullName
                };

                _rabbit.SendMessage(user);

                Thread.Sleep(1000);
            }
        }
    }
}

﻿using RabbitShell;

namespace RabbitReceiver
{
    class Program
    {
        private static readonly string queueName = "HelloRabbitMQ";
        static void Main(string[] args)
        {
            var rabbitReceiver = new Rabbit(queueName);
            rabbitReceiver.ReceiveMessages();
        }
    }
}

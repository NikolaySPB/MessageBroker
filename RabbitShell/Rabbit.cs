﻿using DataShipping;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace RabbitShell
{
    public class Rabbit : IDisposable
    {
        private readonly string _queueName;
        private readonly ConnectionFactory _connectFactory;
        private readonly IConnection _connect;
        private readonly IModel _model;

        public Rabbit(string queueName)
        {
            _queueName = queueName;
            _connectFactory = new ConnectionFactory()
            {
                UserName = "hojkqvye",
                Password = "wJQTJr0J9iRdigOgKNndiXrAXfAfcIH7",
                VirtualHost = "hojkqvye",
                HostName = "bloodhound.rmq.cloudamqp.com"
            };
            _connect = _connectFactory.CreateConnection();
            _model = _connect.CreateModel();
        }

        public void SendMessage(User user)
        {
            _model.QueueDeclare(queue: _queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            var message = JsonConvert.SerializeObject(user);
            var body = Encoding.UTF8.GetBytes(message);

            _model.BasicPublish(exchange: "",
                                     routingKey: _queueName,
                                     basicProperties: null,
                                     body: body);

            Console.WriteLine($" [x] Sent '{message}'");
        }

        public void ReceiveMessages()
        {
            _model.QueueDeclare(queue: _queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            var consumer = new EventingBasicConsumer(_model);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                var user = JsonConvert.DeserializeObject<User>(message);

                Console.WriteLine($" [x] Received '{message}'");
                Console.WriteLine($" User name: '{user.Name}', age: '{user.Age}', email: '{user.Email}'.");
                _model.BasicAck(deliveryTag: ea.DeliveryTag, multiple: true);
            };

            _model.BasicConsume(queue: _queueName,
                                     autoAck: false,
                                     consumer: consumer);

            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }

        public void Dispose()
        {
            _connect?.Dispose();
            _model?.Dispose();
        }
    }
}
